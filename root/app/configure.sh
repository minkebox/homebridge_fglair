#! /bin/sh

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s/{{USERNAME}}/${USERNAME}/g" \
  -e "s/{{PASSWORD}}/${PASSWORD}/g" \
  -e "s/{{MODEL}}/${MODEL}/g" \
  -e "s/{{REGION}}/${REGION}/g" \
  -e "s/{{HA_HOSTNAME}}/${HA_HOSTNAME}/g" \
  -e "s/{{HA_TOKEN}}/${HA_TOKEN}/g" \
  -e "s/{{MII_USERNAME}}/${MII_USERNAME}/g" \
  -e "s/{{MII_PASSWORD}}/${MII_PASSWORD}/g" \
  -e "s/{{MII_REGION}}/${MII_REGION}/g" \
  -e "s/{{SMART_FEELSLIKE}}/${SMART_FEELSLIKE}/g" \
  -e "s/{{SMART_UNIT}}/${SMART_UNIT}/g" \
  -e "s/{{SMART_WEATHER_KEY}}/${SMART_WEATHER_KEY}/g" \
  -e "s/{{SMART_WEATHER_ZIPCODE}}/${SMART_WEATHER_ZIPCODE}/g" \
  > /app/homebridge/config.json
